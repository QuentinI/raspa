"use strict";
const babel = require("gulp-babel");
const coffee = require('gulp-coffee');
const concat = require('gulp-concat');
const gulp = require("gulp");
const gzip = require('gulp-gzip');
const htmlmin = require('gulp-htmlmin');
const sass = require('gulp-sass');
const tar = require('gulp-tar');
const uglify = require('gulp-uglify');

const coffee_opts = {
  bare: true
}

const htmlmin_opts = {
  collapseWhitespace: true
}


const sass_opts = {
  outputStyle: 'compressed'
}

const uglify_opts = {
  preserveComments: 'license'
}

gulp.task("default", ['html', 'coffee', 'sass', 'images', 'pack'], () => {});

gulp.task('coffee', () =>
  gulp.src('./src/main.coffee')
    .pipe(coffee(coffee_opts))
    .pipe(babel())
    .pipe(uglify(uglify_opts))
    .pipe(gulp.dest('./dest/raspa/'))
);

gulp.task("html", () =>
  gulp.src("src/*.html")
    .pipe(htmlmin(htmlmin_opts))
    .pipe(gulp.dest("./dest/raspa/"))
);

gulp.task("images", () =>
  gulp.src("./images/**/*")
    .pipe(gulp.dest("./dest/raspa/images/"))
);

gulp.task('pack', () =>
  gulp.src('./dest/raspa/**/*')
    .pipe(tar('raspa.tar'))
    .pipe(gzip())
    .pipe(gulp.dest('./dest/'))
);

gulp.task('sass', () =>
  gulp.src('./src/*.sass')
    .pipe(sass(sass_opts).on('error', sass.logError))
    .pipe(gulp.dest('./dest/raspa/'))
);
