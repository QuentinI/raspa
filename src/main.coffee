###
@author Geron
@license WTFPL
Это опять ты?! Или не ты... Не важно, вот тебе ещё сердечко.

     ******       ******
   **********   **********
 ************* *************
*****************************
**********кулхацкеру*********
*****************************
 ************от*************
   ***********************
     ******Герона*******
       ***************
         ***********
           *******
             ***
              *

###

Function::property = (prop, desc) ->
    Object.defineProperty @prototype, prop, desc

Array::remove = (obj) ->
    @filter (el) -> el isnt obj

Storage::setObj = (key, obj) ->
    return @setItem key, JSON.stringify(obj)

Storage::getObj = (key) ->
    item = @getItem key
    if item
        return JSON.parse(item)
    return []

setCookie = (cname, cvalue, exdays) ->
    d = new Date
    d.setTime(d.getTime() + (exdays*24*60*60*1000))
    document.cookie = "#{cname}=#{cvalue};expires=#{d.toUTCString()};path=/"


getCookie = (cname) ->
    name = "#{cname}="
    ca = document.cookie.split(';')
    for c in ca
        while c.charAt(0) is ' '
            c = c.substring(1)
        if c.indexOf(name) is 0
            return c.substring(name.length, c.length)
    return ""


class Parser
    constructor : (@backend="papa") ->

    download : (callback, papa=@papa_default, xlsx=@xlsx_default) ->
        if @backend is 'papa'
            @download_papa(callback)
        else
            @download_xlsx()

    download_xlsx : () ->

    download_papa : (callback) ->
        Papa.parse "http://raspa.tk/cors.php",
            download: true,
            dynamicTyping: true,
            comments: "#",
            complete: (results) =>
                @parse(results.data)
                callback(@parsed)

    _clean2d : (array) ->
        result = []
        for row in array
            empty = true
            for value in row
                if value
                    empty = false
                    break
            unless empty
                result.push row
        return result

    _clean : (array) ->
        result = []
        for value in array
            if value
                result.push value
        return result

    _clean_groups : (groups) ->
        for group of groups
            groups[group] = @_clean_lessons groups[group]
        return groups

    _clean_groups_row : (array) ->
        result = {}
        for i in [0..array.length]
            if /(\d|\d\d).-(1|2)/.test array[i]
                result[array[i]] = i
        return result

    _clean_lessons : (array) ->
        for i in [array.length-1..0]
            if array[i].name
                return array[0..i]


    parse : (array) ->
        array = @_clean2d array
        header = @_clean(array[0])[0] # first non-empty cell
        groups = []
        for i in [0..array.length]
            if array[i+1]?[0] is 1 and typeof(array[i][0]) isnt "number" # which means we are on the row with group names
                groupnames = @_clean_groups_row array[i]
                for groupname of groupnames
                    group =
                        name: groupname
                        lessons: []
                    j = i + 1
                    while typeof array[j]?[0] is "number"
                        group.lessons.push
                            order: array[j][0]
                            name: array[j]?[groupnames[groupname]]
                            room: array[j]?[groupnames[groupname]+1]
                        j += 1
                    group.lessons = @_clean_lessons group.lessons
                    groups.push group
        @parsed = {
            header: header
            groups: groups
        }

class Settings
    constructor : ->
        @defaults =
            color:
                name: 'Цвет сайта'
                type: 'string'
                itype: 'dropdown'
                dropdown:
                    'white': 'Белый'
                    'blue': 'Синий'
                    'yellow': 'Желтый'
                    'red': 'Красный'
                    'teal': 'Цвет волны на Гавайях'
                def: 'white'
            inverted:
                name: 'Темная тема'
                type: 'boolean'
                itype: 'checkbox'
                def: false
            client_parse:
                name: 'Включить парсинг на стороне клиента'
                type: 'boolean'
                itype: 'checkbox'
                def: false
        @settings = {}
        @settings.favs = new Favs
        @check_defaults()

    check_defaults : ->
        for setting of @defaults
            unless @settings[setting] and typeof(@settings[setting]) == @defaults[setting]["type"]
                @settings[setting] = @defaults[setting]["def"]

    get_view : ->
        view = document.createElement 'form'
        view.classList.add 'ui', 'form', 'data-item'
        for setting of @defaults
            view.innerHTML += @["_"+@defaults[setting]["itype"]](setting)
        return view

    _dropdown : (i) ->
        opts = ""
        for opt of @defaults[i].dropdown
            opts += "<option value=\"#{opt}\">#{@defaults[i].dropdown[opt]}</option>"
        return """
          <div class="field">
            <label>#{@defaults[i]["name"]}</label>
            <select class="ui fluid dropdown">
                #{opts}
            </select>
          </div>
         """

    _checkbox : (i) ->
        return """
          <div class="field">
            <div class="ui checkbox">
              <input tabindex="0" name=#{i} class="hidden" type="checkbox">
              <label>#{@defaults[i]["name"]}</label>
            </div>
          </div>
         """


class Favs
    constructor : ->
        @load()

    save : ->
        setCookie 'favs', JSON.stringify(@favs), 365

    load : ->
        try
            @favs = JSON.parse getCookie 'favs'
        catch
            @favs = []
            @save()


    add : (name) ->
        unless name in @favs
            @favs.push name
            @save()
        return @favs

    remove : (name) ->
        if name in @favs
            @favs = @favs.remove name
            @save()
        return @favs

    check: (name) ->
        return name in @favs

    toggle: (name) ->
        if name in @favs
            @favs = @favs.remove name
        else
            @favs.push name
        @save()
        return @favs

    get : ->
        return @favs

    isEmpty : ->
        return @favs.length < 1

class View
    constructor : ->
        @settings = new Settings
        if @settings.settings.client_parse
            @parser = new Parser
            @parser.download (data) =>
                @init(data)
        else
            $.get 'http://beta.raspa.tk/parser.php', (data) =>
                @init(data)


    render_view : () ->
        view = document.createElement 'div'
        view.classList.add 'ui', 'four', 'column', 'doubling', 'stackable', 'grid', 'container', 'data-item'
        return view


    init : (data) ->
        @content = document.getElementById 'content'
        @data = data
        @settings_view = @settings.get_view()
        @settings_view.id = 'settings-data'
        @favs_view = @render_view()
        @favs_view.id = 'favs-data'
        @view = @render_view()
        @view.id = 'beautiful-data'
        favs = @settings.settings.favs.get()
        for group in @data
            r = @render_group(group)
            r.id = "group-#{group.name}"
            if group.name in favs
                r2 = r.cloneNode(true)
                r2.id = "group-#{group.name}-fav"
                @favs_view.appendChild(r2)
            @view.appendChild(r)
        document.getElementById('wheelohate').classList.remove('active')
        document.body.appendChild @content
        @content.appendChild @favs_view
        @content.appendChild @view
        @content.appendChild @settings_view
        @clipboard = new Clipboard '.clipboard'
        @clipboard.on 'success', (e) ->
            e.trigger.getElementsByTagName("i")[0].classList.add "green"
            e.trigger.parentElement.setAttribute 'data-tooltip', 'Скопировано'
        document.body.onhashchange = @switch
        if window.location.hash
            @switch()
        else
            window.location.hash = if @settings.settings.favs.isEmpty() then 'beautiful' else 'favs'

    switch : (e) ->
        for item in document.getElementsByClassName 'menu-item'
            item.classList.remove 'active'
        for item in document.getElementsByClassName 'data-item'
            item.classList.add 'data-item-hidden'
        document.getElementById(window.location.hash[1..-1]).classList.add 'active'
        document.getElementById(window.location.hash[1..-1] + '-data').classList.remove 'data-item-hidden'

    render_group : (group) ->
        msgtext = [" " + (lesson.name || "Окно") for lesson in group.lessons].join(",")
        fav = @settings.settings.favs.check(group.name)
        lessons = ""
        (lessons += """
            <tr class="lesson-#{lesson.order}">
                <td>#{lesson.order}</td>
                <td>#{lesson.name or ""}</td>
                <td>#{lesson.room or ""}</td>
            </tr>
        """ for lesson in group.lessons)
        node = document.createElement 'div'
        node.classList.add 'column'
        node.innerHTML = """
            <table class="ui unstackable celled table">
                <thead>
                    <tr>
                        <th colspan="3">
                            <div class="ui ribbon large label">#{group.name}</div>
                            <div class="ui circular large label" data-position="bottom left" data-tooltip="#{if fav then "Удалить из избранного" else "Добавить в избранное"}">
                                <a onclick="view.#{if fav then "remove" else "add"}_fav('#{group.name}'); return false">
                                    <i class="right #{unless fav then "empty"} star icon"></i>
                                </a>
                            </div>
                            <div class="ui circular large label" data-position="bottom left" data-tooltip="Поделиться в Telegram">
                                <a href="https://telegram.me/share/url?url=With ♥ via raspa.tk&text=Расписание для #{group.name}:#{msgtext}" target="_blank">
                                    <i class="right blue send icon"></i>
                                </a>
                            </div>
                            <div class="ui circular large label" data-position="bottom left" data-tooltip="Скопировать">
                                <a class="clipboard" data-clipboard-text="Расписание для #{group.name}:#{msgtext}.\nWith ♥ via raspa.tk">
                                    <i class="right copy icon"></i>
                                </a>
                            </div>
                        </th>
                    </tr>
                <thead>
                <tbody>
                    #{lessons}
                </tbody>
            </table>
        """
        return node

    remove_fav : (name) ->
        unless @settings.settings.favs.check(name)
            return
        @settings.settings.favs.remove(name)
        group = ''
        for g in @data
            if g.name is name
                group = g
        unless group
            return
        old_render = document.getElementById("group-#{group.name}-fav")
        if old_render then @favs_view.removeChild(old_render)
        @update_group_render(group)

    add_fav : (name) ->
        if @settings.settings.favs.check(name)
            return
        @settings.settings.favs.add(name)
        group = ''
        for g in @data
            if g.name is name
                group = g
        unless group
            return
        render = @render_group(group)
        render.id = "group-#{group.name}-fav"
        @favs_view.appendChild(render)
        @update_group_render(group)

    update_group_render : (group) ->
        old_render = document.getElementById("group-#{group.name}")
        render = @render_group(group)
        @view.replaceChild(render, old_render)
        render.id = "group-#{group.name}"



window.view = new View
